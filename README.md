# Ejercicio React 2º DAW - DWEC - Pedro Rodriguez

Requisitos necesarios para iniciar este proyecto:

- NodeJs versión 16.14 o superior

- NPM version 8.3.1 o superior

Para iniciar este proyecto habrá que entrar en la carpeta del proyecto en una terminal.

`cd /ruta/al/proyecto`

`npm install`

`npm start`

Para suplir la falta de backend, esta simulado con un JSON con los articulos y utilizando el local Storage para el carro
