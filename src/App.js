import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Footer from './components/footer/footer'
import Header from './components/header/header'
import About from './components/about/about'
import Product from './components/product/product'
import Home from './components/home/home'
import Detail from './components/detail/detail.js'
import Login from './components/login/login.js'
import SignUp from './components/signup/signup.js'
import CartManagement from './components/cartManagement/cartManagement.js'
import Uploads from './components/uploads/uploads'
import './App.css'

function App () {
  return (
    <BrowserRouter>
      <Header />
        <Routes>
          <Route path="/" element={<Home/>}></Route>
          <Route path="/about" element={<About/>}></Route>
          <Route path="/product" element={<Product/>}></Route>
          <Route path="/product/:productId" element={<Detail/>}></Route>
          <Route path="/login" element={<Login/>}></Route>
          <Route path="/signup" element={<SignUp/>}></Route>
          <Route path="/cart" element={<CartManagement/>}></Route>
          <Route path="/upload" element={<Uploads/>}></Route>
        </Routes>
      <Footer />
    </BrowserRouter>
  )
}

export default App
