const shoppingCart = 'cart'
const user = 'user'
const login = 'login'

function getData () {
  return JSON.parse(localStorage.getItem(shoppingCart))
}

function setData () {
  localStorage.setItem(shoppingCart, JSON.stringify([]))
}

function updateCart (product) {
  const cart = JSON.parse(localStorage.getItem(shoppingCart))
  cart.push(product)
  localStorage.setItem(shoppingCart, JSON.stringify(cart))
}

function deleteCart (i) {
  const cart = JSON.parse(localStorage.getItem(shoppingCart))
  cart.splice(i, 1)
  localStorage.setItem(shoppingCart, JSON.stringify(cart))
}

function clearStorage () {
  localStorage.removeItem(shoppingCart)
}

function createUser () {
  localStorage.setItem(user, JSON.stringify({}))
}

function createLogin () {
  localStorage.setItem(login, JSON.stringify(false))
}

function setLogin () {
  localStorage.setItem(login, JSON.stringify(true))
}

function getLogin () {
  return JSON.parse(localStorage.getItem(login))
}

function getUser () {
  return JSON.parse(localStorage.getItem(user))
}

function setUser (userData) {
  localStorage.setItem(user, JSON.stringify(userData))
}

function logOut () {
  localStorage.removeItem(user)
  createLogin()
}

module.exports = { getData, setData, updateCart, deleteCart, clearStorage, createUser, createLogin, setLogin, getLogin, getUser, setUser, logOut }
