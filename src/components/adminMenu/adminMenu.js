import React from 'react'
import { Link } from 'react-router-dom'
import { getLogin, getUser } from '../../services/localStorageManagement.js'

function AdminMenu () {
  const login = getLogin()
  return (
    <li className="nav-item">
      {
      login && getUser().roles[0].perfil === 'ADMIN'
        ? (
          <Link className="nav-link" to="/upload">
            Nuevo
          </Link>
          )
        : null
      }
    </li>
  )
}

export default AdminMenu
