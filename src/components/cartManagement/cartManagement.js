import React from 'react'
import { getData } from '../../services/localStorageManagement'
import Cart from '../cart/cart'

function CartManagement () {
  const cart = getData()

  return Object.keys(cart).length > 0 ? <Cart cart={cart} /> : <div className="text-center">No hay productos en el carrito</div>
}

export default CartManagement
