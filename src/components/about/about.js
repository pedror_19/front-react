import React from 'react'
import './about.css'

export default function About () {
  return (
    <>
    <div className="h-100 mb-5 p-5 bg-secondary border-secondary rounded-3">
      <h1 className="text-center text-light">Tienda de Pedro</h1>
      <p className="text-center text-light">
        Bienvenido a la tienda de Pedro, tu tienda favorita de camisetas de baloncesto y fútbol americano.
      </p>
    </div>

    <div className="d-flex justify-content-around">
      {
        /*
        * TODO insertar mapa de google maps con la direccion de la tienda
        */
      }
      <div>
        <h1 className="text-center text-light">¿Dónde estamos?</h1>
        <p className="text-center text-light">Rúa José Seoane Rama, 16, 15011 A Coruña</p>
      </div>
      <div className="max-vw-30 nav navbar-dark flex-column ">
        <h2 className="text-light">Nuestras redes sociales y teléfono</h2>
        <ul className="navbar-nav">
          <li className="nav-item text-center">
            <a href="www.instagram.com" className="nav-link fs-3 redes"><i className="bi bi-instagram"></i> Tienda de Pedro</a>
          </li>
          <li className="nav-item text-center">
            <a href="www.twitter.com" className="nav-link fs-3 redes"><i className="bi bi-twitter"></i> Tienda de Pedro</a>
          </li>
          <li className="nav-item text-center">
            <a href="www.facebook.com" className="nav-link fs-3 redes"><i className="bi bi-facebook"></i> Tienda de Pedro</a>
          </li>
        </ul>
        <p className="fs-3 text-center text-light"><i className="bi bi-telephone-fill"></i> 666 666 666</p>
      </div>
    </div>
    </>
  )
}
