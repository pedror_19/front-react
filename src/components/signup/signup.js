import React, { useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { createUser, setData } from '../../services/localStorageManagement.js'
import axios from 'axios'

import './signup.css'

function SignUp () {
  const navigate = useNavigate()

  const [paises, setPaises] = useState({})
  const [formState, setFormState] = useState({
    email: '',
    nombre: '',
    password: '',
    direccion: '',
    tlf: '',
    roles: [
      {
        id: 3
      }
    ]
  })

  useEffect(() => {
    axios.get('http://localhost:8080/pais/')
      .then(res => {
        setPaises(res.data)
      })
  })

  const handleChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.value
    })
  }

  const handleChangeObject = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: {
        id: [e.target.value]
      }
    })
  }

  const onSubmit = (e) => {
    e.preventDefault()

    axios.post('http://localhost:8080/usuario/', formState)
      .then(() => {
        createUser()
        setData()
        navigate('/login', { replace: true })
      })
  }

  return (
    <>
      <h1 className="text-center  mb-2">Sign Up</h1>
      <form className='w-50 m-auto formBorde p-4 d-flex flex-column' onSubmit={onSubmit}>
          <div className="form-group m-2">
            <label className="form-label" htmlFor="nombre">Nombre de usuario</label>
            <input className="form-control-plaintext border-bottom border-light" name="nombre" type="text" placeholder="Nombre de usuario" id="nombre" onChange={handleChange}/>
          </div>
          <div className="form-group m-2">
            <label className="form-label" htmlFor="email">Email</label>
            <input className="form-control-plaintext border-bottom border-light" name="email" type="email" placeholder="Email" id="email" onChange={handleChange}/>
          </div>
          <div className="form-group m-2">
            <label className="form-label" htmlFor="password">Contraseña</label>
              <input className="form-control-plaintext border-bottom border-light" name="password" type="password" placeholder="Contraseña" id="password" onChange={handleChange}/>
          </div>
          <div className="form-group m-2">
            <label className="form-label" htmlFor="direccion">Dirección</label>
            <input className="form-control-plaintext border-bottom border-light" name="direccion" type="text" placeholder="Direccion" id="direccion" onChange={handleChange}/>
          </div>
          <div className="form-group m-2">
            <label className="form-label" htmlFor="tlf">Teléfono</label>
            <input className="form-control-plaintext border-bottom border-light" name="tlf" type="text" placeholder="Teléfono" id="tlf" onChange={handleChange}/>
          </div>
          <div className="form-group m-2 d-flex">
          <label className="form-label d-inline fs-5 col-2" htmlFor="categoria">Liga:</label>
          <select className="form-control d-inline w-75 col-6" name="categoria" type="text" id="categoria" onChange={handleChangeObject}>
            <option value="">Selecciona una liga</option>
            {paises.map((categoria) => {
              return (
                <option key={categoria.id} value={categoria.id}>{categoria.nombre}</option>
              )
            })}
          </select>
        </div>
          <button type="submit" className="btn btn-primary m-2">Sign Up</button>
      </form>
      <Link to="/login" className="nav-link text-center">Ya estás registrado?</Link>
    </>
  )
}

export default SignUp
