import React from 'react'
import { Link } from 'react-router-dom'
import { Image } from 'cloudinary-react'

function ProductList (data) {
  const items = data.products

  return (
    <div className="container-fluid d-flex justify-content-around flex-wrap mt-2 margen">
      {items.map((item) => (
        <div key={item.id} className="card formato p-3">
          <Image cloudName="pedror19" publicId={item.imagen} width="300" crop="scale" className="m-auto esquinasImg"/>
          <div className="card-body d-flex flex-column justify-content-center">
            <h5 className="card-title text-center">{item.nombre}</h5>
            <h5 className="card-title text-center">{item.equipo}</h5>
            <p className="card-text text-center">{ `Liga: ${item.categoria.nombre}` }</p>
            <p className="card-text text-center fw-bold">{`Precio: ${item.precio}€`}</p>
            <Link to={'/product/' + (item.id)} className="btn btn-primary">
              Ver producto
            </Link>
          </div>
        </div>
      ))}
    </div>
  )
}

export default ProductList
