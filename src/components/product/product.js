import React, { useEffect, useState } from 'react'
import ProductList from '../productList/productList'
import axios from 'axios'
import { Image } from 'cloudinary-react'

export default function Product () {
  const [items, setItems] = useState([])
  const [filter, setFilter] = useState('')
  const [categoria, setCategoria] = useState([])

  const nba = 'https://res.cloudinary.com/pedror19/image/upload/v1653064597/nba/vivunl0h5ulbhzkicwzw.svg'
  const nfl = 'https://res.cloudinary.com/pedror19/image/upload/v1653064598/nba/uoaug5ydlecehvtyofhp.svg'

  useEffect(() => {
    axios.get('http://localhost:8080/producto/').then((response) => {
      if (filter === '') {
        setItems(response.data)
        console.log(filter)
      } else {
        setItems(response.data.filter(item => item.categoria.nombre === filter))
        console.log(filter)
      }
    })
  }, [filter])

  useEffect(() => {
    axios.get('http://localhost:8080/categoria/').then((response) => {
      setCategoria(response.data)
      console.log({ categoria })
    })
  }, [])

  return items
    ? (
      <>
        <div className="h-100 p-5 bg-secondary border-secondary rounded-3 d-flex flex-column justify-content-center">
          <h2 className="text-light mx-auto">Productos</h2>
          <p className="text-light mx-auto my-2">
            Aqui están nuestros productos
          </p>
          <p className="d-flex justify-content-center gap-1">
            <button onClick={() => setFilter(categoria[0].nombre)} className="btn btn-outline-light" type="button">
              <Image cloudName="pedror19" publicId={nba} width="40" crop="scale" className="m-auto" />
            </button>
            <button onClick={() => setFilter(categoria[1].nombre)} className="btn btn-outline-light" type="button">
              <Image cloudName="pedror19" publicId={nfl} width="40" crop="scale" className="m-auto" />
            </button>
            <button onClick={() => setFilter('')} className="btn btn-outline-light" type="button">
              <i className="bi bi-x-lg"></i>
            </button>
          </p>
        </div>
        <ProductList products={items}/>
      </>
      )
    : <div className="vw-100 mt-5 d-flex justify-content-center align-content-center"><div className="spinner-border" role="status"></div></div>
}
