import React, { useEffect, useState } from 'react'
import { deleteCart, clearStorage, setData } from '../../services/localStorageManagement'
import './cart.css'
import { Image } from 'cloudinary-react'

function deleteItemCart (i) {
  deleteCart(i)
  location.reload()
}

function pagar () {
  clearStorage()
  setData()
  location.reload()
}

function Cart (data) {
  const [totalPrice, setTotalPrice] = useState(0)
  const [cart, setCart] = useState([])

  useEffect(() => {
    setCart(data.cart)
    let total = 0
    cart.forEach(item => {
      total += item.precio
    })
    setTotalPrice(total)
  }, [data.cart])

  return (
    <div className="container margenCarro">
      {cart.map((item, key) => {
        return (
          <div key={key} className="card w-75 mx-auto m-5">
            <div className="card-header">{item.nombre}</div>
            <div className="card-body d-flex flex-row">
              <div className="w-25">
                <Image cloudName="pedror19" publicId={item.imagen} width="300" crop="scale" className="card-img w-100 esquinasImg"/>
              </div>
              <div className="d-flex mx-auto flex-column justify-content-center w-50">
                <p className="card-text text-center">{`Liga: ${item.categoria.nombre}`}</p>
                <p className="card-text text-center">{`Edición: ${item.opcion.nombre}`}</p>
                <p className="card-text text-center">{`Precio: ${item.precio}€`}</p>
                <button onClick={() => deleteItemCart(key)} className="btn btn-primary w-50 m-auto">Borrar</button>
              </div>
            </div>
          </div>
        )
      })}
      <div className="d-flex flex-column justify-content-between">
        <p className="text-center">{`Total: ${totalPrice}€`}</p>
        <button onClick={() => pagar()} className="btn btn-primary w-50 m-auto">Pagar</button>
      </div>
    </div>
  )
}

export default Cart
