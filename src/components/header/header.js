import React from 'react'
import { Link } from 'react-router-dom'
import LoginMenu from '../loginMenu/loginMenu.js'
import AdminMenu from '../adminMenu/adminMenu.js'

import './header.css'
import { Image } from 'cloudinary-react'

export default function Header () {
  const logo = 'https://res.cloudinary.com/pedror19/image/upload/v1653069500/nba/czwyrctprtgvjpmssxl3.svg'

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <Link to="/" className="navbar-brand" >
          <Image cloudName="pedror19" publicId={logo} crop="scale" className=" mt-1 logo nav-item text-light"/>
        </Link>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarColor01">
          <ul className="navbar-nav me-auto">
            <li className="nav-item">
              <Link to="/product" className="nav-link" >
                Productos
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/about" className="nav-link" >
                About
              </Link>
            </li>
            <AdminMenu />
          </ul>
          <form className="d-flex mx-1">
            <input className="form-control me-sm-2" type="text" placeholder="Search" />
            <button className="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
          </form>
          <ul className="navbar-nav justify-content-end">
            <li className="nav-item mx-1">
              <Link className="nav-link" aria-current="page" to="/cart">
                <i className="bi bi-cart2 fs-4"></i>
              </Link>
            </li>
            <LoginMenu />
          </ul>
        </div>
      </div>
    </nav>
  )
}
