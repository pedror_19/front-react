import React from 'react'
import { Link } from 'react-router-dom'

export default function footer () {
  return (
    <footer className="footer py-2 fixed-bottom bg-dark">
      <ul className="nav justify-content-center border-bottom border-secondary pb-3 mb-3">
        <li className="nav-item">
          <Link to="/" className="nav-link px-2">
            Home
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/products" className="nav-link px-2">
            Productos
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/about" className="nav-link px-2">
            About
          </Link>
        </li>
      </ul>
      <p className="text-center text-ligth">© 2022 Pedro Rodriguez</p>
    </footer>
  )
}
