import React, { useState } from 'react'
import UploadForm from '../uploadForm/uploadForm'
import PaisForm from '../paisForm/paisForm'
import PedidoForm from '../pedidoForm/pedidoForm'
import OpcionForm from './../opcionForm/opcionForm'
import CategoriaForm from '../categoriaFrom/categoriaForm'

import './uploads.css'

export default function Uploads () {
  const [elemento, setElemento] = useState('')

  const handleChange = (e) => {
    setElemento(e.target.value)
  }

  return (
    <>
      <div className="h-100 p-5 bg-secondary border-secondary rounded-3 d-flex flex-column justify-content-center">
          <h2 className="text-light mx-auto">Nuevo</h2>
          <p className="text-light mx-auto my-2">
            Selecciona que quieres crear
          </p>
          <form className="d-flex justify-content-center gap-1">
            <select className="form-control d-inline w-25 col-6" name="form" id="form" onChange={(e) => handleChange(e)}>
              <option value="">Selecciona que quieres crear</option>
              <option value="pais">País</option>
              <option value="producto">Producto</option>
              <option value="pedido">Pedido</option>
              <option value="opcion">Opción</option>
              <option value="categoria">Categoría</option>
            </select>
          </form>
        </div>
        <div className="formMargin">
        {
          elemento === 'producto'
            ? <UploadForm/>
            : elemento === 'pais'
              ? <PaisForm/>
              : elemento === 'pedido'
                ? <PedidoForm/>
                : elemento === 'opcion'
                  ? <OpcionForm/>
                  : elemento === 'categoria'
                    ? <CategoriaForm/>
                    : <div className="vw-100 mt-5 d-flex justify-content-center align-content-center"><div className="spinner-border" role="status"></div></div>
        }
        </div>
    </>
  )
}
