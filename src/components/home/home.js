import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import ProductList from '../productList/productList'

import './home.css'

export default function Home () {
  const [items, setItems] = useState([])

  useEffect(() => {
    axios.get('http://localhost:8080/producto/').then((response) => {
      setItems(response.data)
    })
  }, [])

  return (
    <>
      <div className="h-100 p-5 bg-secondary border-secondary rounded-3 d-flex flex-column justify-content-center">
        <h2 className="text-light mx-auto">Tienda de Pedro</h2>
        <p className="text-light mx-auto my-2">
          Bienvenido a tu tienda favorita de camisetas de baloncesto
        </p>
        <Link to="/about" className="btn btn-outline-light w-25 m-auto d-block" type="button">
          Más sobre nosotros
        </Link>
      </div>
      <div className="container-fluid d-flex justify-content-around flex-wrap mt-2 margen">
        <ProductList products={items}/>
      </div>
    </>
  )
}
