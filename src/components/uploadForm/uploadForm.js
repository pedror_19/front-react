import React, { useState, useEffect } from 'react'
import axios from 'axios'

export default function uploadForm () {
  const [imgFiles, setImgFiles] = useState('')
  const [opciones, setOpciones] = useState([])
  const [categoria, setCategoria] = useState([])
  const [formState, setFormState] = useState({
    nombre: '',
    precio: 0,
    equipo: '',
    descripcion: '',
    imagen: '',
    categoria: 0,
    opciones: 0
  })

  useEffect(() => {
    axios.get('http://localhost:8080/opcion/')
      .then(res => {
        setOpciones(res.data)
      })
  }, [])

  useEffect(() => {
    axios.get('http://localhost:8080/categoria/')
      .then(res => {
        setCategoria(res.data)
      })
  }, [])

  const handleChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.value
    })
  }

  const handleChangeObject = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: {
        id: e.target.value
      }
    })
  }

  const uploadImage = (e) => {
    e.preventDefault()
    const data = new FormData()
    data.append('file', imgFiles)
    data.append('upload_preset', 'c2wkbjn5')
    axios
      .post('https://api.cloudinary.com/v1_1/pedror19/image/upload', data)
      .then((res) => {
        setFormState({
          ...formState,
          imagen: res.data.url
        })
        createProduct()
      }).catch(
        (err) => console.log(err)
      )
  }

  const createProduct = (e) => {
    e.preventDefault()
    console.log({ formState })

    axios.post('http://localhost:8080/producto/', formState)
      .then((res) => {
        console.log(res)
      }).catch((err) => console.log(err))
  }

  return (
    <>
      <h1 className="text-center my-5">Nuevo producto</h1>
      <form className="w-50 formBorde p-4 mx-auto d-flex flex-column">
        <div className="form-group m-2 d-flex">
          <label className="form-label d-inline fs-5 col-2" htmlFor="nombre">Nombre:</label>
          <input className="form-control d-inline w-75 col-6" name="nombre" type="text" id="nombre" onChange={handleChange} />
        </div>
        <div className="form-group m-2 d-flex">
          <label className="form-label fs-5 col-2" htmlFor="precio">Precio:</label>
          <input className="form-control w-75 col-6" name="precio" type="number" id="precio" onChange={handleChange} />
        </div>
        <div className="form-group m-2 d-flex">
          <label className="form-label d-inline fs-5 col-2" htmlFor="equipo">Equipo:</label>
          <input className="form-control d-inline w-75 col-6" name="equipo" type="text" id="equipo" onChange={handleChange} />
        </div>
        <div className="form-group m-2 d-flex">
          <label className="form-label d-inline fs-5 col-2" htmlFor="descripcion">Descripción:</label>
          <input className="form-control d-inline w-75 col-6" name="descripcion" type="text" id="descripcion" onChange={handleChange} />
        </div>
        <div className="form-group m-2 d-flex">
          <label className="form-label d-inline fs-5 col-2" htmlFor="categoria">Liga:</label>
          <select className="form-control d-inline w-75 col-6" name="categoria" type="text" id="categoria" onChange={handleChangeObject}>
            <option value="">Selecciona una liga</option>
            {categoria.map((categoria) => {
              return (
                <option key={categoria.id} value={categoria.id}>{categoria.nombre}</option>
              )
            })}
          </select>
        </div>
        <div className="form-group m-2 d-flex">
          <label className="form-label d-inline fs-5 col-2" htmlFor="opcion">Edición:</label>
          <select className="form-control d-inline w-75 col-6" name="opcion" type="text" id="opcion" onChange={handleChangeObject}>
          <option value="">Selecciona una edición</option>
            {opciones.map((opcion) => {
              return (
                <option key={opcion.id} value={opcion.id}>{opcion.nombre}</option>
              )
            })}
          </select>
        </div>

        <div className="form-group m-2 d-flex">
          <div className="col-6 w-75">
            <input
              className="form-control "
              type="file"
              placeholder="Email"
              id="username"
              onChange={(e) => setImgFiles(e.target.files[0])}
            />
          </div>
          <button className="btn btn-primary mx-2 col-2" onClick={ (e) => uploadImage(e) }>Subir</button>
        </div>
        <button
          type="submit"
          className="btn btn-primary m-2 w-75 mx-auto"
          onClick={ (e) => createProduct(e) }
        >
          Crear
        </button>
      </form>
    </>
  )
}
