import React, { useState, useEffect } from 'react'
import axios from 'axios'

export default function PedidoForm () {
  const [usuarios, setUsuarios] = useState([])
  const [pedido, setPedido] = useState({
    precioTotal: 0,
    direccionEnvio: '',
    usuario: {}
  })

  useEffect(() => {
    axios.get('http://localhost:8080/usuario/')
      .then(res => {
        setUsuarios(res.data)
        console.log(res.data)
      })
  }, [])

  const handleChange = (e) => {
    setPedido({
      ...pedido,
      [e.target.name]: e.target.value
    })
  }

  const handleChangeObject = (e) => {
    setPedido({
      ...pedido,
      [e.target.name]: {
        id: e.target.value
      }
    })
  }

  const createPedido = (e) => {
    e.preventDefault()
    axios.post('http://localhost:8080/pedido/', pedido)
  }

  return (
    <>
    <h1 className="text-center my-5">Nuevo pedido</h1>
    <form className="w-50 formBorde p-4 mx-auto d-flex flex-column">
      <div className="form-group m-2 d-flex">
        <label className="form-label fs-5 col-2" htmlFor="precioTotal">Precio:</label>
        <input className="form-control w-75 col-6" name="precioTotal" type="number" id="precioTotal" onChange={handleChange} />
      </div>
      <div className="form-group m-2 d-flex">
        <label className="form-label fs-5 col-2" htmlFor="direccionEnvio">Dirección:</label>
        <input className="form-control w-75 col-6" name="direccionEnvio" type="text" id="direccionEnvio" onChange={handleChange} />
      </div>
      <div className="form-group m-2 d-flex">
        <label className="form-label d-inline fs-5 col-2" htmlFor="usuario">Usuario:</label>
        <select className="form-control d-inline w-75 col-6" name="usuario" type="text" id="usuario" onChange={handleChangeObject}>
          <option value="">Selecciona el usuario</option>
          {usuarios.map((usuario) => {
            return (
              <option key={usuario.id} value={usuario.id}>{usuario.nombre}</option>
            )
          })}
        </select>
      </div>
      <button
        type="submit"
        className="btn btn-primary m-2 w-75 mx-auto"
        onClick={ (e) => createPedido(e) }
      >
        Crear
      </button>
    </form>
  </>
  )
}
