import React from 'react'
import { Link } from 'react-router-dom'
import { getLogin, logOut } from '../../services/localStorageManagement.js'

function closeSession () {
  logOut()
  window.location = '/'
}

function LoginMenu () {
  const login = getLogin()
  return (
    <li className="nav-item">
      {
      login
        ? (
          <a className="nav-link mx-1" onClick={() => closeSession()}>
            <i className="bi bi-door-open fs-4"></i>
          </a>
          )
        : (
          <Link className="nav-link" to="/login">
            <i className="bi bi-person-circle fs-4"></i>
          </Link>
          )
      }
    </li>
  )
}

export default LoginMenu
