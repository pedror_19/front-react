import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { setUser, setLogin, createLogin, getLogin, setData } from '../../services/localStorageManagement.js'
import axios from 'axios'

function Login () {
  const [formState, setFormState] = useState({
    nombre: '',
    password: ''
  })

  const handleChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.value
    })
  }

  const onSubmit = (e) => {
    e.preventDefault()
    const login = getLogin()
    if (!login) {
      axios.post('http://localhost:8080/usuario/login/', formState)
        .then(res => {
          setUser(res.data)
          setData()
          createLogin()
          setLogin()
          window.location = '/'
        })
    } else {
      alert('Ya estás logueado')
    }
  }

  return (
    <>
      <h1 className="text-center mb-2">Login</h1>
      <form className='w-50 formBorde p-4 m-auto d-flex flex-column' onSubmit={onSubmit}>
          <div className="form-group m-2">
            <label className="form-label" htmlFor="nombre">Nombre de usuario</label>
            <input className="form-control-plaintext border-bottom border-light" name="nombre" type="text" placeholder="Nombre de usuario" id="nombre" onChange={handleChange}/>
          </div>
          <div className="form-group m-2">
            <label className="form-label" htmlFor="password">Contraseña</label>
              <input className="form-control-plaintext border-bottom border-light" name="password" type="password" placeholder="Contraseña" id="password" onChange={handleChange}/>
          </div>
          <button type="submit" className="btn btn-primary m-2">Sign Up</button>
      </form>
      <Link to="/signup" className="nav-link text-center">Si aun no tienes cuenta registrate</Link>
    </>
  )
}

export default Login
