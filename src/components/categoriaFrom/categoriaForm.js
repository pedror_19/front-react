import React, { useState } from 'react'
import axios from 'axios'

export default function CategoriaForm () {
  const [categoria, setCategoria] = useState({
    nombre: '',
    descripcion: ''
  })
  const handleChange = (e) => {
    setCategoria({
      ...categoria,
      [e.target.name]: e.target.value
    })
  }
  const createCategoria = (e) => {
    e.preventDefault()
    axios.post('http://localhost:8080/categoria/', categoria)
      .catch((e) => console.log(e))
  }
  return (
    <>
      <h1 className="text-center my-5">Nueva Opcion</h1>
        <form className="w-50 formBorde p-4 mx-auto d-flex flex-column">
          <div className="form-group m-2 d-flex">
            <label className="form-label d-inline fs-5 col-2" htmlFor="nombre">Nombre:</label>
            <input className="form-control d-inline w-75 col-6" name="nombre" type="text" id="nombre" onChange={handleChange} />
          </div>
          <div className="form-group m-2 d-flex">
            <label className="form-label d-inline fs-5 col-2" htmlFor="descripcion">Descipción:</label>
            <input className="form-control d-inline w-75 col-6" name="descripcion" type="text" id="descripcion" onChange={handleChange} />
          </div>
          <button
            type="submit"
            className="btn btn-primary m-2 w-75 mx-auto"
            onClick={ (e) => createCategoria(e) }
          >
            Crear
          </button>
        </form>
      </>
  )
}
