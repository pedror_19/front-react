import React, { useState } from 'react'
import axios from 'axios'

export default function PaisForm () {
  const [pais, setPais] = useState({})
  const handleChange = (e) => {
    setPais({
      ...pais,
      [e.target.name]: e.target.value
    })
  }
  const createOpcion = (e) => {
    e.preventDefault()
    axios.post('http://localhost:8080/pais/', pais)
      .catch((e) => console.log(e))
  }
  return (
    <>
      <h1 className="text-center my-5">Nuevo Pais</h1>
        <form className="w-50 formBorde p-4 mx-auto d-flex flex-column">
          <div className="form-group m-2 d-flex">
            <label className="form-label d-inline fs-5 col-2" htmlFor="nombre">Nombre:</label>
            <input className="form-control d-inline w-75 col-6" name="nombre" type="text" id="nombre" onChange={handleChange} />
          </div>
          <button
            type="submit"
            className="btn btn-primary m-2 w-75 mx-auto"
            onClick={ (e) => createOpcion(e) }
          >
            Crear
          </button>
        </form>
      </>
  )
}
