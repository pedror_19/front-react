import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import { updateCart } from '../../services/localStorageManagement'
import { Image } from 'cloudinary-react'

export default function Detail () {
  const [item, setItems] = useState(null)

  const { productId } = useParams()

  const update = () => {
    updateCart(item)
  }

  useEffect(() => {
    axios.get(`http://localhost:8080/producto/${productId}`).then((response) => {
      setItems(response.data)
    })
  }, [])

  return item
    ? (
        <div className="card w-75 mx-auto my-3">
          <div className="card-header fs-4">{item.nombre}</div>
          <div className="card-body d-flex flex-row justify-content-around">
            <p className="w-25"><Image cloudName="pedror19" publicId={item.imagen} width="300" crop="scale" className="m-auto"/></p>
            <div className="d-flex flex-column justify-content-center w-50">
              <h2 className="card-text m-auto">{`Precio: ${item.precio}€`}</h2>
              <p className="card-text m-auto fs-5">{item.descripcion}</p>
              <p className="card-text m-auto fs-5">{`Liga: ${item.categoria.nombre}`}</p>
              <p className="card-text m-auto fs-5">{`Edición: ${item.opcion.nombre}`}</p>
              <button onClick={update} className="btn btn-primary w-50 py-3 m-auto">Añadir al carro</button>
            </div>
          </div>
        </div>
      )
    : <div className="vw-100 mt-5 d-flex justify-content-center align-content-center"><div className="spinner-border" role="status"></div></div>
}
